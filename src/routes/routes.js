import DashboardLayout from '../layout/DashboardLayout.vue'
import contact from '../layout/contact.vue'


const routes = [
  {
    path: '/',
    component: DashboardLayout,
  },
  {
    path: '/contact',
    component: contact,
  },
]

export default routes
